<?php
  require 'db.php';

  $formats = [];
  $errs = [];
  $pdo = DBConnector::connect();

  $sql = 'SELECT * from formats';
  foreach ($pdo->query($sql) as $key => $row) {
    $formats[$key]['id'] = $row['id'];
    $formats[$key]['name'] = $row['name'];
  }

  DBConnector::disconnect();

if (!empty($_POST)) {
      $fields  =  array('name', 'format', 'release_date','actors' );
      $subfields  =  array('fname', 'lname' );
      foreach ($fields as $f) {
          if (empty($_POST[$f])){
            $errs[$f] = 'Field cannot be emty!';
          }elseif ($f == 'release_date' && !is_int($_POST[$f]) && (($_POST[$f] > intval(date("Y")) ) 
                                        || ($_POST[$f] < 1900) ) 
                  ) {
            $errs[$f] = 'Date must be digit number, less than 1900 and not bigger than current year!';
          }elseif ($f == 'actors'){
              foreach ($subfields as $sf) {
                  if (empty($_POST[$f][0][$sf])){
                      $errs[$sf] = 'Field cannot be emty!';
                  }
              }
          }
          elseif ($f = 'name') {
            $sort = null;
            switch ($_POST[$f]) {
              case preg_match('/^[0-9\s\-\:,.:?$]+$/', $name):
                $sort = 0;
              break;
              case preg_match('/^[a-zA-Z0-9\s\-\:,.:?$]+$/', $name):
                $sort = 1;
              break;
              case preg_match('/^[\p{Cyrillic}0-9\s\-\:,.:?$]+$/u', $name):
                $sort = 2;
              break;
              default: $errs[$f] = 'en/ru/uk names are allowed without special symbols!'; break;
            }
          }
      }
 // var_dump($errs);
    if (empty($errs)) {
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "INSERT INTO movies (name, release_date, format_id, sort_param) values(?, ?, ?, ?)";
      $query = $pdo->prepare($sql);
      $query->execute([$_POST['name'], $_POST['release_date'], $_POST['format'], $sort]);
      $lastMovieID = $pdo->lastInsertId();
      $actors = $_POST['actors'];
      // var_dump($actors);
      foreach ($actors as $actor) {
          $sql = "INSERT INTO actors (fname, lname, movie_id) values(?, ?, ?)";
          $query = $pdo->prepare($sql);
          $query->execute([$actor['fname'], $actor['lname'], $lastMovieID]);
      }
      DBConnector::disconnect();
      header('Location: index.php');
    }
}
$title = 'Add movie';
include('views/add.php');