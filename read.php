<?php
require 'db.php';

$id = 0;
if (!empty($_GET['id'])) {
  $id = $_GET['id'];
}

if ($id === null) {
  header('Location: index.php');
} else {
  $pdo = DBConnector::connect();

  //movies
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "SELECT * FROM movies WHERE id = ?";
  $query = $pdo->prepare($sql);
  $query->execute([$id]);
  $movie = $query->fetch(PDO::FETCH_ASSOC);

  //formats
  $sql = "SELECT name FROM formats WHERE id = ?";
  $query = $pdo->prepare($sql);
  $query->execute([$movie['format_id']]);
  $format = $query->fetch(PDO::FETCH_ASSOC);

  //actors
  $sql = "SELECT * FROM actors WHERE movie_id = ?";
  $query = $pdo->prepare($sql);
  $query->execute([$id]);
  $actors = $query->fetchAll();
  DBConnector::disconnect();
}

$title = 'Info about '.$movie['name'].' movie';
include('views/read.php');
