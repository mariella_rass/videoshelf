<?php

class DBConnector {

  private static $DB_NAME = 'video_shelf';
  private static $DB_HOST = 'localhost';
  private static $DB_USER = 'root';
  private static $DB_PASS = '';
  private static $CONFIG = null;

  public function __construct() {
    die('No init here is allowed.');
  }

  //connect to db
  public static function connect() {
    if (self::$CONFIG == null) {
      try {
        self::$CONFIG = new PDO("mysql:host=".self::$DB_HOST . ";" . "dbname=" . self::$DB_NAME, self::$DB_USER, self::$DB_PASS);
      } catch (PDOException $e) {
        die($e->getMessage());
      }
    }
    return self::$CONFIG;
  }

  // disconnect from db
  public static function disconnect() {
    self::$CONFIG = null;
  }

}
