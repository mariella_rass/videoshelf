<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="container">
            <div class="row">
              <div class="well text-center"><h2 style="margin-top: 10px;"><?=$title?></h2></div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-right" style="margin-bottom: 10px;">
                <div id="sort-block" class="col-sm-6 text-left">
                  <a href="add.php" class="btn btn-success" type="button">Add</a>
                  <a href="index.php?sort_by=1" class="btn btn-info" type="button">Sort</a>
                  <a href="upload.php" class="btn btn-default" type="button">Upload</a>
                </div>
                <div id="search-block" class="col-sm-6">
                  <form class="form-inline" action="index.php" method="post">
                    <select class="form-control" name="search_by">
                        <option value="title">Search by title</option>
                        <option value="name">Search by actor name</option>
                    </select>
                    <div class="form-group">
                      <input type="text" class="form-control" name="search_param" placeholder="Title/Actor">
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                  </form>
                </div>
              </div>
              <div class="col-sm-12">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Release Date</th>
                      <th>Format</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach($data as $d) {?>
                      <tr>
                          <td><?=$d['name'];?></td>
                          <td><?=$d['release_date'];?></td>
                          <td><?=$d['format'];?></td>
                          <td>
                            <a class="btn btn-default" href="read.php?id=<?=$d['id']?>" style="width: 100%">Info</a>
                          </td>
                      </tr>
                    <?php } ?>
                  </tbody>
            </table>
          </div>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
