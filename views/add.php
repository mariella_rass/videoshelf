<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="container">
      <div class="col-sm-6 center-block c-float">
        <div class="row">
          <div class="well text-center"><h2><?=$title?></h2></div>
        </div>
      <form class="form-horizontal" action="add.php" method="post">
        <div class="form-group">
          <label class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10 <?= $class = (!empty($errs['name']))  ? 'has-error' : '' ?>">
            <input name="name" class="form-control" type="text" aria-describedby="errorBlock1"  placeholder="Title" value="<?=$_POST['name']?>">
             <?php if ( !empty($errs['name']) ) {?>
                <span id="errorBlock3" class="help-block"><?= $errs['name'] ?></span>
            <?php } ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Release</label>
          <div class="col-sm-10 <?= $class = (!empty($errs['release_date']))  ? 'has-error' : '' ?>">
            <input name="release_date" class="form-control" type="text" aria-describedby="errorBlock2" placeholder="Release date" value="<?=$_POST['release_date']?>">
                <?php if (!(empty($errs['release_date']))) {?>
                    <span id="errorBlock2" class="help-block"><?= $errs['release_date'] ?></span>
                <?php } ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Format</label>
          <div class="col-sm-10">
            <select class="form-control" name="format">
               <?php foreach ($formats as $format) {?>
                    <option value="<?=$format['id']?>"> <?=$format['name']?></option>
               <? } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-panel text-right">Actors</label>
          <div class="col-sm-5 <?= $class = (!empty($errs['fname']))  ? 'has-error' : '' ?>">
            <input name="actors[0][fname]" class="form-control" type="text" aria-describedby="errorBlock3" placeholder="Actor's first name" value="<?=$_POST['actors'][0]['fname']?>">
               <?php if (!empty($errs['fname'])) {?>
                    <span id="errorBlock3" class="help-block"><?=$errs['fname']?></span>
               <?php } ?>
          </div>
          <div class="col-sm-5 <?= $class = (!empty($errs['lname']))  ? 'has-error' : '' ?>">
            <input name="actors[0][lname]" class="form-control" type="text" aria-describedby="errorBlock4" placeholder="Actor's last name" value="<?=$_POST['actors'][0]['lname']?>">
                <?php if (!empty($errs['lname'])) {?>
                    <span id="errorBlock4" class="help-block"><?=$errs['lname']?></span>
                <?php } ?>
          </div>
        </div>
        <div id="new-actor" class="form-group">
            <div class="col-sm-12 text-right">
                <button id="act-add" type="button" class="btn btn-primary">Add an actor</button>
            </div>
        </div>
        <div class="btn-group btn-group-justified">
          <div class="btn-group" role="group">
            <button type="submit" class="btn btn-success">Add</button>
          </div>
          <div class="btn-group" role="group">
            <a class="btn btn-default" type="button" href="index.php">Back</a>
          </div>
        </div>
      </form>
    </div>
  </div> 
  <script src="js/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $(document).ready(function() {
      var count = 0;
      $('#new-actor #act-add').on('click', function() {
        count = count + 1;
        var newActorBlock =
        '<div class="form-group">' +
          '<div class="col-sm-2"></div>' +
          '<div class="col-sm-5">' +
              '<input name="actors[' + count + '][fname]" class="form-control" type="text" placeholder="First name">' +
          '</div>' +
          '<div class="col-sm-5">' +
              '<input name="actors[' + count + '][lname]" class="form-control" type="text" placeholder="Last name">' +
          '</div>' +
        '</div>';
        $(newActorBlock).insertBefore('#new-actor');
      });
    });
  </script>
  </body>
</html>
