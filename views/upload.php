<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sweetalert.css" rel="stylesheet">
</head>

<body>
    <div class="container">
      <div class="row">
          <div class="col-sm-6 text-center center-block" style="width:100%">
              <div class="well text-center"><h2><?=$title?></h2></div>
          </div>
      </div>
      <div class="row">
        <div class="col-sm-6 center-block" style="width:100%; text-align: center;">
            <form class="well" action="upload.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="file">Select a file to upload</label>
                    <input type="file" name="file" style="display: inline-block;"/>
                    <p class="help-block">Only txt file with maximum size of 1MB allowed.</p>
                </div>
                <button class="btn btn-lg btn-primary" type="submit">Upload</button>
                <a href="index.php" class="btn btn-lg btn-default" type="button">Back</a>
            </form>
        </div>
      </div>
      <div class="row">
        <?php if( count($files) > 0 ) { ?>
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>File name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($result as $file) { ?>
                    <tr>
                      <td><?=$file?></td>
                      <td>
                        <a class="btn btn-primary" href="dbupload.php?name=<?=$file?>">Upload to DB</a>
                        <a class="btn btn-danger" href="remove.php?name=<?=$file?>">Delete</a>
                      </td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
        <?php } ?>
      </div>
  </div> <!-- /container -->
  <script src="js/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/sweetalert.min.js"></script>
  </body>
</html>
