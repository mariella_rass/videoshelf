<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sweetalert.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
      <div class="row" style="margin-top: 1%;">
        <div class="well text-center"><h2 style="margin-top: 10px;"><?=$title?></h2></div>
      </div>
      <div id="action-buttons" class="row">
        <p>
          <a href="index.php" class="btn btn-success" type="button">Back to list</a>
          <a id="delete-movie" href="#" class="btn btn-danger" type="button">Delete the movie</a>
        </p>
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Title</th>
              <th>Release Date</th>
              <th>Format</th>
              <th>Actors</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td> <?=$movie['name']?> </td>
              <td> <?=$movie['release_date']?> </td>
              <td> <?=$format['name'] ?> </td>
              <td>
                <ul>
                  <?php foreach ($actors as $actor) { ?>
                      <li> <?=$actor['fname'].' '.$actor['lname']?></li>
                  <?php } ?>
                </ul>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
  </div> <!-- /container -->
  <script src="js/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/sweetalert.min.js"></script>
  <script src="js/custom.js"></script>
  </body>
</html>
