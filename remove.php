<?php

$fname = $_GET['name'];
$fpath = 'uploads/' . $fname;

if (file_exists($fpath)) {
  unlink($fpath);
  header('Location: upload.php');
}
