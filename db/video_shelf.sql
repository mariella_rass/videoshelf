-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 22 2018 г., 13:51
-- Версия сервера: 5.7.13
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `video_shelf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `actors`
--

CREATE TABLE IF NOT EXISTS `actors` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `movie_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=700 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `actors`
--

INSERT INTO `actors` (`id`, `fname`, `lname`, `movie_id`) VALUES
(351, 'Mel', 'Brooks', 78),
(352, 'Clevon', 'Little', 78),
(353, 'Harvey', 'Korman', 78),
(354, 'Gene', 'Wilder', 78),
(355, 'Slim', 'Pickens', 78),
(356, 'Madeline', 'Kahn', 78),
(357, 'Humphrey', 'Bogart', 79),
(358, 'Ingrid', 'Bergman', 79),
(359, 'Claude', 'Rains', 79),
(360, 'Peter', 'Lorre', 79),
(361, 'Audrey', 'Hepburn', 80),
(362, 'Cary', 'Grant', 80),
(363, 'Walter', 'Matthau', 80),
(364, 'James', 'Coburn', 80),
(365, 'George', 'Kennedy', 80),
(366, 'Paul', 'Newman', 81),
(367, 'George', 'Kennedy', 81),
(368, 'Strother', 'Martin', 81),
(369, 'Paul', 'Newman', 82),
(370, 'Robert', 'Redford', 82),
(371, 'Katherine', 'Ross', 82),
(372, 'Robert', 'Redford', 83),
(373, 'Paul', 'Newman', 83),
(374, 'Robert', 'Shaw', 83),
(375, 'Charles', 'Durning', 83),
(376, 'Jim', 'Henson', 84),
(377, 'Frank', 'Oz', 84),
(378, 'Dave', 'Geolz', 84),
(379, 'Mel', 'Brooks', 84),
(380, 'James', 'Coburn', 84),
(381, 'Charles', 'Durning', 84),
(382, 'Austin', 'Pendleton', 84),
(383, 'John', 'Travolta', 85),
(384, 'Danny', 'DeVito', 85),
(385, 'Renne', 'Russo', 85),
(386, 'Gene', 'Hackman', 85),
(387, 'Dennis', 'Farina', 85),
(388, 'Joe', 'Pesci', 86),
(389, 'Marrisa', 'Tomei', 86),
(390, 'Fred', 'Gwynne', 86),
(391, 'Austin', 'Pendleton', 86),
(392, 'Lane', 'Smith', 86),
(393, 'Ralph', 'Macchio', 86),
(394, 'Russell', 'Crowe', 87),
(395, 'Joaquin', 'Phoenix', 87),
(396, 'Connie', 'Nielson', 87),
(397, 'Harrison', 'Ford', 88),
(398, 'Mark', 'Hamill', 88),
(399, 'Carrie', 'Fisher', 88),
(400, 'Alec', 'Guinness', 88),
(401, 'James', 'Earl', 88),
(402, 'Harrison', 'Ford', 89),
(403, 'Karen', 'Allen', 89),
(404, 'Nathan', 'Fillion', 90),
(405, 'Alan', 'Tudyk', 90),
(406, 'Adam', 'Baldwin', 90),
(407, 'Ron', 'Glass', 90),
(408, 'Jewel', 'Staite', 90),
(409, 'Gina', 'Torres', 90),
(410, 'Morena', 'Baccarin', 90),
(411, 'Sean', 'Maher', 90),
(412, 'Summer', 'Glau', 90),
(413, 'Chiwetel', 'Ejiofor', 90),
(414, 'Gene', 'Hackman', 91),
(415, 'Barbara', 'Hershey', 91),
(416, 'Dennis', 'Hopper', 91),
(417, 'Matthew', 'Broderick', 92),
(418, 'Ally', 'Sheedy', 92),
(419, 'Dabney', 'Coleman', 92),
(420, 'John', 'Wood', 92),
(421, 'Barry', 'Corbin', 92),
(422, 'Bill', 'Pullman', 93),
(423, 'John', 'Candy', 93),
(424, 'Mel', 'Brooks', 93),
(425, 'Rick', 'Moranis', 93),
(426, 'Daphne', 'Zuniga', 93),
(427, 'Joan', 'Rivers', 93),
(428, 'Gene', 'Wilder', 94),
(429, 'Kenneth', 'Mars', 94),
(430, 'Terri', 'Garr', 94),
(431, 'Gene', 'Hackman', 94),
(432, 'Peter', 'Boyle', 94),
(433, 'Val', 'Kilmer', 95),
(434, 'Gabe', 'Jarret', 95),
(435, 'Michelle', 'Meyrink', 95),
(436, 'William', 'Atherton', 95),
(437, 'Tom', 'Cruise', 96),
(438, 'Kelly', 'McGillis', 96),
(439, 'Val', 'Kilmer', 96),
(440, 'Anthony', 'Edwards', 96),
(441, 'Tom', 'Skerritt', 96),
(442, 'Donald', 'Sutherland', 97),
(443, 'Elliot', 'Gould', 97),
(444, 'Tom', 'Skerritt', 97),
(445, 'Sally', 'Kellerman', 97),
(446, 'Robert', 'Duvall', 97),
(447, 'Carl', 'Reiner', 98),
(448, 'Eva', 'Marie', 98),
(449, 'Alan', 'Arkin', 98),
(450, 'Brian', 'Keith', 98),
(451, 'Roy', 'Scheider', 99),
(452, 'Robert', 'Shaw', 99),
(453, 'Richard', 'Dreyfuss', 99),
(454, 'Lorraine', 'Gary', 99),
(455, 'Keir', 'Dullea', 100),
(456, 'Gary', 'Lockwood', 100),
(457, 'William', 'Sylvester', 100),
(458, 'Douglas', 'Rain', 100),
(459, 'James', 'Stewart', 101),
(460, 'Josephine', 'Hull', 101),
(461, 'Peggy', 'Dow', 101),
(462, 'Charles', 'Drake', 101),
(463, 'Seth', 'Rogen', 102),
(464, 'Katherine', 'Heigl', 102),
(465, 'Paul', 'Rudd', 102),
(466, 'Leslie', 'Mann', 102),
(467, 'Mel', 'Brooks', 103),
(468, 'Clevon', 'Little', 103),
(469, 'Harvey', 'Korman', 103),
(470, 'Gene', 'Wilder', 103),
(471, 'Slim', 'Pickens', 103),
(472, 'Madeline', 'Kahn', 103),
(473, 'Humphrey', 'Bogart', 104),
(474, 'Ingrid', 'Bergman', 104),
(475, 'Claude', 'Rains', 104),
(476, 'Peter', 'Lorre', 104),
(477, 'Audrey', 'Hepburn', 105),
(478, 'Cary', 'Grant', 105),
(479, 'Walter', 'Matthau', 105),
(480, 'James', 'Coburn', 105),
(481, 'George', 'Kennedy', 105),
(482, 'Paul', 'Newman', 106),
(483, 'George', 'Kennedy', 106),
(484, 'Strother', 'Martin', 106),
(485, 'Paul', 'Newman', 107),
(486, 'Robert', 'Redford', 107),
(487, 'Katherine', 'Ross', 107),
(488, 'Robert', 'Redford', 108),
(489, 'Paul', 'Newman', 108),
(490, 'Robert', 'Shaw', 108),
(491, 'Charles', 'Durning', 108),
(492, 'Jim', 'Henson', 109),
(493, 'Frank', 'Oz', 109),
(494, 'Dave', 'Geolz', 109),
(495, 'Mel', 'Brooks', 109),
(496, 'James', 'Coburn', 109),
(497, 'Charles', 'Durning', 109),
(498, 'Austin', 'Pendleton', 109),
(499, 'John', 'Travolta', 110),
(500, 'Danny', 'DeVito', 110),
(501, 'Renne', 'Russo', 110),
(502, 'Gene', 'Hackman', 110),
(503, 'Dennis', 'Farina', 110),
(504, 'Joe', 'Pesci', 111),
(505, 'Marrisa', 'Tomei', 111),
(506, 'Fred', 'Gwynne', 111),
(507, 'Austin', 'Pendleton', 111),
(508, 'Lane', 'Smith', 111),
(509, 'Ralph', 'Macchio', 111),
(510, 'Russell', 'Crowe', 112),
(511, 'Joaquin', 'Phoenix', 112),
(512, 'Connie', 'Nielson', 112),
(513, 'Harrison', 'Ford', 113),
(514, 'Mark', 'Hamill', 113),
(515, 'Carrie', 'Fisher', 113),
(516, 'Alec', 'Guinness', 113),
(517, 'James', 'Earl', 113),
(518, 'Harrison', 'Ford', 114),
(519, 'Karen', 'Allen', 114),
(520, 'Nathan', 'Fillion', 115),
(521, 'Alan', 'Tudyk', 115),
(522, 'Adam', 'Baldwin', 115),
(523, 'Ron', 'Glass', 115),
(524, 'Jewel', 'Staite', 115),
(525, 'Gina', 'Torres', 115),
(526, 'Morena', 'Baccarin', 115),
(527, 'Sean', 'Maher', 115),
(528, 'Summer', 'Glau', 115),
(529, 'Chiwetel', 'Ejiofor', 115),
(530, 'Gene', 'Hackman', 116),
(531, 'Barbara', 'Hershey', 116),
(532, 'Dennis', 'Hopper', 116),
(533, 'Matthew', 'Broderick', 117),
(534, 'Ally', 'Sheedy', 117),
(535, 'Dabney', 'Coleman', 117),
(536, 'John', 'Wood', 117),
(537, 'Barry', 'Corbin', 117),
(538, 'Bill', 'Pullman', 118),
(539, 'John', 'Candy', 118),
(540, 'Mel', 'Brooks', 118),
(541, 'Rick', 'Moranis', 118),
(542, 'Daphne', 'Zuniga', 118),
(543, 'Joan', 'Rivers', 118),
(544, 'Gene', 'Wilder', 119),
(545, 'Kenneth', 'Mars', 119),
(546, 'Terri', 'Garr', 119),
(547, 'Gene', 'Hackman', 119),
(548, 'Peter', 'Boyle', 119),
(549, 'Val', 'Kilmer', 120),
(550, 'Gabe', 'Jarret', 120),
(551, 'Michelle', 'Meyrink', 120),
(552, 'William', 'Atherton', 120),
(553, 'Tom', 'Cruise', 121),
(554, 'Kelly', 'McGillis', 121),
(555, 'Val', 'Kilmer', 121),
(556, 'Anthony', 'Edwards', 121),
(557, 'Tom', 'Skerritt', 121),
(558, 'Donald', 'Sutherland', 122),
(559, 'Elliot', 'Gould', 122),
(560, 'Tom', 'Skerritt', 122),
(561, 'Sally', 'Kellerman', 122),
(562, 'Robert', 'Duvall', 122),
(563, 'Carl', 'Reiner', 123),
(564, 'Eva', 'Marie', 123),
(565, 'Alan', 'Arkin', 123),
(566, 'Brian', 'Keith', 123),
(567, 'Roy', 'Scheider', 124),
(568, 'Robert', 'Shaw', 124),
(569, 'Richard', 'Dreyfuss', 124),
(570, 'Lorraine', 'Gary', 124),
(571, 'Keir', 'Dullea', 125),
(572, 'Gary', 'Lockwood', 125),
(573, 'William', 'Sylvester', 125),
(574, 'Douglas', 'Rain', 125),
(575, 'James', 'Stewart', 126),
(576, 'Josephine', 'Hull', 126),
(577, 'Peggy', 'Dow', 126),
(578, 'Charles', 'Drake', 126),
(579, 'Seth', 'Rogen', 127),
(580, 'Katherine', 'Heigl', 127),
(581, 'Paul', 'Rudd', 127),
(582, 'Leslie', 'Mann', 127),
(583, 'Mel', 'Brooks', 128),
(584, 'Clevon', 'Little', 128),
(585, 'Harvey', 'Korman', 128),
(586, 'Gene', 'Wilder', 128),
(587, 'Slim', 'Pickens', 128),
(588, 'Madeline', 'Kahn', 128),
(589, 'Humphrey', 'Bogart', 129),
(590, 'Ingrid', 'Bergman', 129),
(591, 'Claude', 'Rains', 129),
(592, 'Peter', 'Lorre', 129),
(593, 'Audrey', 'Hepburn', 130),
(594, 'Cary', 'Grant', 130),
(595, 'Walter', 'Matthau', 130),
(596, 'James', 'Coburn', 130),
(597, 'George', 'Kennedy', 130),
(598, 'Paul', 'Newman', 131),
(599, 'George', 'Kennedy', 131),
(600, 'Strother', 'Martin', 131),
(601, 'Paul', 'Newman', 132),
(602, 'Robert', 'Redford', 132),
(603, 'Katherine', 'Ross', 132),
(604, 'Robert', 'Redford', 133),
(605, 'Paul', 'Newman', 133),
(606, 'Robert', 'Shaw', 133),
(607, 'Charles', 'Durning', 133),
(608, 'Jim', 'Henson', 134),
(609, 'Frank', 'Oz', 134),
(610, 'Dave', 'Geolz', 134),
(611, 'Mel', 'Brooks', 134),
(612, 'James', 'Coburn', 134),
(613, 'Charles', 'Durning', 134),
(614, 'Austin', 'Pendleton', 134),
(615, 'John', 'Travolta', 135),
(616, 'Danny', 'DeVito', 135),
(617, 'Renne', 'Russo', 135),
(618, 'Gene', 'Hackman', 135),
(619, 'Dennis', 'Farina', 135),
(620, 'Joe', 'Pesci', 136),
(621, 'Marrisa', 'Tomei', 136),
(622, 'Fred', 'Gwynne', 136),
(623, 'Austin', 'Pendleton', 136),
(624, 'Lane', 'Smith', 136),
(625, 'Ralph', 'Macchio', 136),
(626, 'Russell', 'Crowe', 137),
(627, 'Joaquin', 'Phoenix', 137),
(628, 'Connie', 'Nielson', 137),
(629, 'Harrison', 'Ford', 138),
(630, 'Mark', 'Hamill', 138),
(631, 'Carrie', 'Fisher', 138),
(632, 'Alec', 'Guinness', 138),
(633, 'James', 'Earl', 138),
(634, 'Harrison', 'Ford', 139),
(635, 'Karen', 'Allen', 139),
(636, 'Nathan', 'Fillion', 140),
(637, 'Alan', 'Tudyk', 140),
(638, 'Adam', 'Baldwin', 140),
(639, 'Ron', 'Glass', 140),
(640, 'Jewel', 'Staite', 140),
(641, 'Gina', 'Torres', 140),
(642, 'Morena', 'Baccarin', 140),
(643, 'Sean', 'Maher', 140),
(644, 'Summer', 'Glau', 140),
(645, 'Chiwetel', 'Ejiofor', 140),
(646, 'Gene', 'Hackman', 141),
(647, 'Barbara', 'Hershey', 141),
(648, 'Dennis', 'Hopper', 141),
(649, 'Matthew', 'Broderick', 142),
(650, 'Ally', 'Sheedy', 142),
(651, 'Dabney', 'Coleman', 142),
(652, 'John', 'Wood', 142),
(653, 'Barry', 'Corbin', 142),
(654, 'Bill', 'Pullman', 143),
(655, 'John', 'Candy', 143),
(656, 'Mel', 'Brooks', 143),
(657, 'Rick', 'Moranis', 143),
(658, 'Daphne', 'Zuniga', 143),
(659, 'Joan', 'Rivers', 143),
(660, 'Gene', 'Wilder', 144),
(661, 'Kenneth', 'Mars', 144),
(662, 'Terri', 'Garr', 144),
(663, 'Gene', 'Hackman', 144),
(664, 'Peter', 'Boyle', 144),
(665, 'Val', 'Kilmer', 145),
(666, 'Gabe', 'Jarret', 145),
(667, 'Michelle', 'Meyrink', 145),
(668, 'William', 'Atherton', 145),
(669, 'Tom', 'Cruise', 146),
(670, 'Kelly', 'McGillis', 146),
(671, 'Val', 'Kilmer', 146),
(672, 'Anthony', 'Edwards', 146),
(673, 'Tom', 'Skerritt', 146),
(674, 'Donald', 'Sutherland', 147),
(675, 'Elliot', 'Gould', 147),
(676, 'Tom', 'Skerritt', 147),
(677, 'Sally', 'Kellerman', 147),
(678, 'Robert', 'Duvall', 147),
(679, 'Carl', 'Reiner', 148),
(680, 'Eva', 'Marie', 148),
(681, 'Alan', 'Arkin', 148),
(682, 'Brian', 'Keith', 148),
(683, 'Roy', 'Scheider', 149),
(684, 'Robert', 'Shaw', 149),
(685, 'Richard', 'Dreyfuss', 149),
(686, 'Lorraine', 'Gary', 149),
(687, 'Keir', 'Dullea', 150),
(688, 'Gary', 'Lockwood', 150),
(689, 'William', 'Sylvester', 150),
(690, 'Douglas', 'Rain', 150),
(691, 'James', 'Stewart', 151),
(692, 'Josephine', 'Hull', 151),
(693, 'Peggy', 'Dow', 151),
(694, 'Charles', 'Drake', 151),
(695, 'Seth', 'Rogen', 152),
(696, 'Katherine', 'Heigl', 152),
(697, 'Paul', 'Rudd', 152),
(698, 'Leslie', 'Mann', 152),
(699, 'wefwef', 'wefwef', 153);

-- --------------------------------------------------------

--
-- Структура таблицы `formats`
--

CREATE TABLE IF NOT EXISTS `formats` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `formats`
--

INSERT INTO `formats` (`id`, `name`) VALUES
(1, 'VHS'),
(2, 'DVD'),
(3, 'Blu-Ray');

-- --------------------------------------------------------

--
-- Структура таблицы `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `release_date` int(4) DEFAULT NULL,
  `format_id` int(11) DEFAULT NULL,
  `sort_param` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `movies`
--

INSERT INTO `movies` (`id`, `name`, `release_date`, `format_id`, `sort_param`) VALUES
(78, 'Blazing Saddles', 1974, 1, 1),
(79, 'Casablanca', 1942, 2, 1),
(80, 'Charade', 1953, 2, 1),
(81, 'Cool Hand Luke', 1967, 1, 1),
(82, 'Butch Cassidy and the Sundance Kid', 1969, 1, 1),
(83, 'The Sting', 1973, 2, 1),
(84, 'The Muppet Movie', 1979, 2, 1),
(85, 'Get Shorty ', 1995, 2, 1),
(86, 'My Cousin Vinny', 1992, 2, 1),
(87, 'Gladiator', 2000, 3, 1),
(88, 'Star Wars', 1977, 3, 1),
(89, 'Raiders of the Lost Ark', 1981, 2, 1),
(90, 'Serenity', 2005, 3, 1),
(91, 'Hooisers', 1986, 1, 1),
(92, 'WarGames', 1983, 1, 1),
(93, 'Spaceballs', 1987, 2, 1),
(94, 'Young Frankenstein', 1974, 1, 1),
(95, 'Real Genius', 1985, 1, 1),
(96, 'Top Gun', 1986, 2, 1),
(97, 'MASH', 1970, 2, 1),
(98, 'The Russians Are Coming, The Russians Are Coming', 1966, 1, 1),
(99, 'Jaws', 1975, 2, 1),
(100, '2001', 1968, 2, 0),
(101, 'Harvey', 1950, 2, 1),
(102, 'Knocked Up', 2007, 3, 1),
(103, 'Blazing Saddles', 1974, 1, 1),
(104, 'Casablanca', 1942, 2, 1),
(105, 'Charade', 1953, 2, 1),
(106, 'Cool Hand Luke', 1967, 1, 1),
(107, 'Butch Cassidy and the Sundance Kid', 1969, 1, 1),
(108, 'The Sting', 1973, 2, 1),
(109, 'The Muppet Movie', 1979, 2, 1),
(110, 'Get Shorty ', 1995, 2, 1),
(111, 'My Cousin Vinny', 1992, 2, 1),
(112, 'Gladiator', 2000, 3, 1),
(113, 'Star Wars', 1977, 3, 1),
(114, 'Raiders of the Lost Ark', 1981, 2, 1),
(115, 'Serenity', 2005, 3, 1),
(116, 'Hooisers', 1986, 1, 1),
(117, 'WarGames', 1983, 1, 1),
(118, 'Spaceballs', 1987, 2, 1),
(119, 'Young Frankenstein', 1974, 1, 1),
(120, 'Real Genius', 1985, 1, 1),
(121, 'Top Gun', 1986, 2, 1),
(122, 'MASH', 1970, 2, 1),
(123, 'The Russians Are Coming, The Russians Are Coming', 1966, 1, 1),
(124, 'Jaws', 1975, 2, 1),
(125, '2001', 1968, 2, 0),
(126, 'Harvey', 1950, 2, 1),
(127, 'Knocked Up', 2007, 3, 1),
(128, 'Blazing Saddles', 1974, 1, 1),
(129, 'Casablanca', 1942, 2, 1),
(130, 'Charade', 1953, 2, 1),
(131, 'Cool Hand Luke', 1967, 1, 1),
(132, 'Butch Cassidy and the Sundance Kid', 1969, 1, 1),
(133, 'The Sting', 1973, 2, 1),
(134, 'The Muppet Movie', 1979, 2, 1),
(135, 'Get Shorty ', 1995, 2, 1),
(136, 'My Cousin Vinny', 1992, 2, 1),
(137, 'Gladiator', 2000, 3, 1),
(138, 'Star Wars', 1977, 3, 1),
(139, 'Raiders of the Lost Ark', 1981, 2, 1),
(140, 'Serenity', 2005, 3, 1),
(141, 'Hooisers', 1986, 1, 1),
(142, 'WarGames', 1983, 1, 1),
(143, 'Spaceballs', 1987, 2, 1),
(144, 'Young Frankenstein', 1974, 1, 1),
(145, 'Real Genius', 1985, 1, 1),
(146, 'Top Gun', 1986, 2, 1),
(147, 'MASH', 1970, 2, 1),
(148, 'The Russians Are Coming, The Russians Are Coming', 1966, 1, 1),
(149, 'Jaws', 1975, 2, 1),
(150, '2001', 1968, 2, 0),
(151, 'Harvey', 1950, 2, 1),
(152, 'Knocked Up', 2007, 3, 1),
(153, 'fwef', 1920, 1, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Индексы таблицы `formats`
--
ALTER TABLE `formats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `format_id` (`format_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `actors`
--
ALTER TABLE `actors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=700;
--
-- AUTO_INCREMENT для таблицы `formats`
--
ALTER TABLE `formats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `actors`
--
ALTER TABLE `actors`
  ADD CONSTRAINT `actors_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`);

--
-- Ограничения внешнего ключа таблицы `movies`
--
ALTER TABLE `movies`
  ADD CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`format_id`) REFERENCES `formats` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
