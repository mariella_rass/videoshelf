<?php
  require 'db.php';

  $movie = 0;

  if (!empty($_POST['id'])) {
      $movie = $_POST['id'];
  }

  if (!empty($_POST)) {
      $pdo = DBConnector::connect();
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $sql = "DELETE FROM actors WHERE movie_id = ?";
      $query = $pdo->prepare($sql);
      $query->execute([$movie]);

      $sql = "DELETE FROM movies WHERE id = ?";
      $query = $pdo->prepare($sql);
      $query->execute([$movie]);
      DBConnector::disconnect();
      echo 1;
  } else {
      echo 0;
  }
