$(document).ready(function() {
      $('#delete-movie').click(function() {
        swal({
          title: "Are you sure?",
          text: "You'll be not able to recover this movie!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "Cansel!",
          closeOnConfirm: false,
          closeOnCancel: false,
        },
        function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              type: 'POST',
              url: 'delete.php',
              data: { id: <?= $movie['id'] ?> },
              success: function(response) {
                if (parseInt(response)) {
                  window.location = "index.php";
                } else {
                  swal("An error occured!", "", "error");
                }
              }
            });
          } else {
            swal("Cancelled!", "Your movie is safe.", "success");
          }
        }
      );
      });
    });