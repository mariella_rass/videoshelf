<?php

require 'db.php';

// sort out line endings
ini_set('auto_detect_line_endings', true);

$fname = $_GET['name'];
$fpath = 'uploads/' . $fname;

function checkMovieTitle($movieName) {
  if (preg_match('/^[0-9\s\-\:,.:?$]+$/', $movieName)) {
    $sort = 0;
  } else if (preg_match('/^[a-zA-Z0-9\s\-\:,.:?$]+$/', $movieName)) {
    $sort = 1;
  } else if (preg_match('/^[\p{Cyrillic}0-9\s\-\:,.:?$]+$/u', $movieName)) {
    $sort = 2;
  } else {
    $sort = 3;
  }
  return $sort;
}

if (file_exists($fpath)) {
  // read the file into an array
  $file = file($fpath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
  $movieInfo = [];

  $pdo = DBConnector::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $formats = [
    1 => 'VHS',
    2 => 'DVD',
    3 => 'Blu-Ray'
  ];

  $sql = "INSERT INTO movies (name, release_date, format_id, sort_param) values(?, ?, ?, ?)";
  $query = $pdo->prepare($sql);

  // for rows counting
  $count = 0;
  for ($i = 0; $i < count($file); $i++) {
    $infoMovie[] = explode(': ', $file[$i]);
    $count++;
    if ($count == 4) {
      $sort = 0;
      $movieTitleCheck = checkMovieTitle($infoMovie[0][1]);
      if($movieTitleCheck !== 3) {
        $sort = $movieTitleCheck;
      } else {
        throw new Exception('Only English, Ukrainian and Russian names are allowed without special symbols!');
      }
      // format name to index in format DB table
      $infoMovie[2][1] = array_search($infoMovie[2][1], $formats);
      $query->execute([$infoMovie[0][1], $infoMovie[1][1], $infoMovie[2][1], $sort]);
      $lastMovieID = $pdo->lastInsertId();


      $actors = explode(', ', $infoMovie[3][1]);
      $sql = [];
      $question_marks = [];
      $insert_values = [];
      foreach ($actors as $actor) {
        $tmpActors = explode(' ', $actor);
        $temp = [
          'fname' => $tmpActors[0],
          'lname' => $tmpActors[1],
          'movie_id' => $lastMovieID
        ];
        $question_marks[] = '(?, ?, ?)';
        $insert_values = array_merge($insert_values, array_values($temp));
        $temp = null;
      }

      $actorSql = "INSERT INTO actors (fname, lname, movie_id) VALUES " . implode(', ', $question_marks);
      $queryA = $pdo->prepare($actorSql);

      try {
          $queryA->execute($insert_values);
      } catch (PDOException $e){
          echo $e->getMessage();
      }

      $count = 0;
      unset($infoMovie);
    }
  }
}

header('Location: index.php');
