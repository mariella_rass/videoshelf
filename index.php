<?php
require 'db.php';

$pdo = DBConnector::connect();

//sorting
$sql = (empty($_GET['sort_by'])) ? "SELECT * FROM movies ORDER BY id DESC" :
       "SELECT * FROM movies ORDER BY sort_param, name collate utf8_unicode_ci";
//searching

if (!empty($_POST['search_by']) && !empty($_POST['search_param'])) {
    $searchParam = $_POST['search_param'];
    $searchBy = $_POST['search_by'];

    if ($searchBy == "title" && $searchParam != "") {
       $sql = "SELECT * FROM movies WHERE name LIKE '{$searchParam}' collate utf8_unicode_ci";
    }

    if ($searchBy == "name" && $searchParam != "") {
       $sql = "SELECT movies.id, name, release_date, format_id
        FROM movies JOIN actors ON movies.id = actors.movie_id WHERE fname LIKE '{$searchParam}' collate utf8_unicode_ci";
    }
}

$data = [];

foreach ($pdo->query($sql) as $key => $row) {
         $fQ = $pdo->query("SELECT `name` FROM `formats` where `id` = {$row['format_id']} LIMIT 1");
         $format = $fQ->fetch(PDO::FETCH_ASSOC); //get format
         $data[$key]['id'] = $row['id'];
         $data[$key]['name'] = $row['name'];
         $data[$key]['release_date'] = $row['release_date'];
         $data[$key]['format'] = $format['name'];
}


DBConnector::disconnect();

$title = 'Video shelf';

include('views/index.php');