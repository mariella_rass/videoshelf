# VideoShelf #

Is a web appliction, writed using php, jQuery and Bootstrap. Main functionality is about storing info about movies, use basic CRUD, search, upload info from file.

### Installation guid ###

To get repositary, in your cmd:

`$ git clone https://mariella_rass@bitbucket.org/mariella_rass/videoshelf.git`

`$ git pull`

###To import db:###

`$ mysql -u <username> - p <yourpass>;`

  `mysql> create database video_shelf;`
  
  `mysql> quit;`
  
`$ mysql -u ` `<username>` ` -p movies < VideoShelf/database/video_shelf.sql;`

###To run app:###

`$ php -S localhost:8000 -t videoshelf/`

###Project structure###

`css`     - folder contains style sheets based on Bootstrap 3;

`db`      - folder contains mysql dump;

`fonts`   - folder contains fonts used in app;

`js`      - folder contains scripts used in app;

`uploads` - upload dir for sample_movies, and other files with movies;

`views`   - views dir;


php files, that not in folders represent basic CRUD and file upload logic.
